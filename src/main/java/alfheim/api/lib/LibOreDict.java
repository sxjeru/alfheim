package alfheim.api.lib;

public class LibOreDict {
	
	public static final String ELVORIUM_INGOT = "ingotElvorium";
	public static final String MAUFTRIUM_INGOT = "ingotMauftrium";
	public static final String MUSPELHEIM_POWER_INGOT = "ingotMuspelheimPower";
	public static final String NIFLHEIM_POWER_INGOT = "ingotNiflheimPower";
	public static final String ELVORIUM_NUGGET = "nuggetElvorium";
	public static final String MAUFTRIUM_NUGGET = "nuggetMauftrium";
	public static final String MUSPELHEIM_ESSENCE = "essenceMuspelheim";
	public static final String NIFLHEIM_ESSENCE = "essenceNiflheim";
	public static final String IFFESAL_DUST = "dustIffesal";
	public static final String[] ARUNE = { "runePrimalA", "runeMuspelheimA", "runeNiflheimA"};
}
