package alfheim.common.world.dim.alfheim;

import alfheim.AlfheimCore;
import alfheim.common.core.utils.AlfheimConfig;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.chunk.IChunkProvider;

public class WorldProviderAlfheim extends WorldProvider {

    @Override
    public void registerWorldChunkManager() {
        this.worldChunkMgr = new WorldChunkManagerAlfheim(this.worldObj);
        this.dimensionId = AlfheimConfig.dimensionIDAlfheim;
        isHellWorld = false;
    }

    @Override
    public float getCloudHeight() {
        return 128.0F;
    }

    @Override
    public IChunkProvider createChunkGenerator() {
        return new ChunkProviderAlfheim(this.worldObj, this.worldObj.getSeed());
    }

    @Override
    public boolean isSurfaceWorld() {
        return true;
    }

    @Override
    public float calculateCelestialAngle(long var1, float var3) {
    	int j = (int)(var1 % 24000L);
        float f1 = ((float)j + var3) / 24000.0F - 0.25F;

        if (f1 < 0.0F) {
            ++f1;
        }

        if (f1 > 1.0F) {
            --f1;
        }

        float f2 = f1;
        f1 = 1.0F - (float)((Math.cos((double)f1 * Math.PI) + 1.0D) / 2.0D);
        f1 = f2 + (f1 - f2) / 3.0F;
        return f1;
    }

    @Override
    public boolean canRespawnHere() {
        return AlfheimCore.enableElvenStory || AlfheimConfig.enableAlfheimRespawn;
    }
    
    @Override
    public String getDimensionName() {
        return "Alfheim";
    }
}
