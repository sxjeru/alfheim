package baubles.api;

public enum BaubleType {
	RING, AMULET, BELT;
	private BaubleType() {}
}